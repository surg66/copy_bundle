name                       = "Copy bundle"
version                    = "1.0.0"
description                = "Version: "..version .."\nCopies bundle from first slot in inventory."
author                     = "surg"
forumthread                = ""
api_version_dst            = 10
icon_atlas                 = "modicon.xml"
icon                       = "modicon.tex"
priority                   = 0
dont_starve_compatible     = false
reign_of_giants_compatible = false
shipwrecked_compatible     = false
hamlet_compatible          = false
dst_compatible             = true
all_clients_require_mod    = false
client_only_mod            = true
server_filter_tags         = {}

local key_list = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","TAB","CAPSLOCK","LSHIFT","RSHIFT","LCTRL","RCTRL","LALT","RALT","ALT","CTRL","SHIFT","SPACE","ENTER","ESCAPE","MINUS","EQUALS","BACKSPACE","PERIOD","SLASH","LEFTBRACKET","BACKSLASH","RIGHTBRACKET","TILDE","PRINT","SCROLLOCK","PAUSE","INSERT","HOME","DELETE","END","PAGEUP","PAGEDOWN","UP","DOWN","LEFT","RIGHT","KP_DIVIDE","KP_MULTIPLY","KP_PLUS","KP_MINUS","KP_ENTER","KP_PERIOD","KP_EQUALS"}
local key_options = {}

for i = 1, #key_list do
    key_options[i] = { description = key_list[i], data = "KEY_"..key_list[i] }
end

configuration_options =
{
    {
        name = "COPY_KEY",
        label = "Copy key",
        hover = "Key to copy bundle",
        options = key_options,
        default = "KEY_O",
    },
    {
        name = "WRAP_TO_GIFT",
        label = "Wrap to gift",
        hover = "Result wrap to gift",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = false,
    },
}
